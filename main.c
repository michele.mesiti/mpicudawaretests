#ifdef __GNUC__
#define _POSIX_C_SOURCE 200809L // not to have warning on posix memalign
#endif

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <openacc.h>
#include <assert.h>
#include <unistd.h>
#include <cuda_runtime.h>

int main(){

    // creating an array 
    const int size = 10; //8*8*8*10;
    const int comm_data_size = 1;//8*8*8;
    int rank,rank2;
    double *a;

    // OpenACC initialisation
    {
        char* localRankStr = NULL;
        if ((localRankStr = getenv("OMPI_COMM_WORLD_RANK")) != NULL){
            printf("LocalRankStr: %s\n",localRankStr);
            rank2 = atoi(localRankStr);
            //rank2 = getpid()%2;
        }
        acc_init(acc_device_nvidia);
        acc_set_device_num(rank2,acc_device_nvidia);
        printf("Setting Rank - OpenACC : %d\n", rank2);
    }

    // mpi initialisation
    {
        int nranks;
        MPI_Init(NULL,NULL);
        MPI_Comm_size(MPI_COMM_WORLD,&nranks);
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        printf("Setting Rank - MPI : %d\n", rank);
        assert(2==nranks);
    }
 
    // Debugging hook
    {
        volatile int flag = 0;
        if(0==rank && getenv("GDBHOOK")){
            printf("Thank you for using the GDB HOOK!\n"
                   "Waiting for user intervention.\n"  
                   "Please attach to process %d,\n",getpid());
            printf("[hint: gdb -p %d ] \n",  getpid());
            printf("and set the value of 'flag' to 1.\n"  
                   "You may have to use 'finish' a couple of times"
                   " to go down the call stack.\n"
                   "HAPPY HUNTING AND GOOD LUCK!!!!\n");
            while(1 != flag)
                sleep(1);
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    // data allocation (on device) and initialisation 
    {

        int i;
        // POSIX Memalign is correlated with a crash on MPI_Finalize()
        //int allocation_check = posix_memalign((void**) &a, 128, size);
        a = malloc(size*sizeof(double));
#pragma acc enter data create(a[0:size])

        printf("MPI%d, device %d, a %p\n", rank, rank2, a);

        for(i=0;i<size;++i) a[i] = i;
#pragma acc update device(a[0:size])
    }

    // communication
    {
        MPI_Request sr,rr;
#pragma acc host_data use_device(a) // REMOVE THIS, NO PROBLEM.
        {
            printf("DEVICE: MPI%d, device %d, a %p\n", rank, rank2, a);
            MPI_Isend((void*) a,    // buf
                    comm_data_size, // count
                    MPI_DOUBLE,     // datatype
                    1-rank,         // destination
                    0,              // tag
                    MPI_COMM_WORLD, // communicator
                    &sr);           // request
            MPI_Irecv((void*) &a[size-comm_data_size], // buf
                    comm_data_size,                    // count
                    MPI_DOUBLE,                        // datatype
                    1-rank,                            // source
                    0,                                 // tag
                    MPI_COMM_WORLD,                    // communicator
                    &rr);                              // request
        }
        MPI_Wait(&sr,MPI_STATUS_IGNORE);
        MPI_Wait(&rr,MPI_STATUS_IGNORE);
    }

    //print
    {
        printf("MPI%02d:First Element of a: %f\n",rank,a[0]);
        printf("MPI%02d:Last  Element of a: %f\n",rank,a[size-1]);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    // shutdown
    {
        printf("MPI%02d:finalising 1\n",rank);
        MPI_Finalize(); // CRASH HERE WHEN posix_memalign is used.
        printf("MPI%02d:finalising 2\n",rank);
        //acc_shutdown(acc_device_nvidia);
        free(a);
    }
    return 0;

}
