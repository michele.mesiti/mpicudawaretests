
CC=pgcc
#CC_FLAGS=-O0 -I${MPIINC} -g
CC_FLAGS=-acc=noautopar -v -O0 -Minfo=all -ta=tesla:cc35,cuda10.0 -I${MPIINC} -g
#LINKER_FLAGS=-O0 -lmpi -L${MPILIB} -g
LINKER_FLAGS=-acc=noautopar -v -O0 -Minfo=all -ta=tesla:cc35,cuda10.0 -lmpi -L${MPILIB} -g
####################################################################################

all : main

main.o: main.c Makefile
	$(CC) -c $(CC_FLAGS) ./main.c

main: main.o
	$(CC) -o main main.o $(LINKER_FLAGS) 

clean:
	rm main main.o
