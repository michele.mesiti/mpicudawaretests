#!/bin/bash
###
#SBATCH -t 1
#SBATCH --gres=gpu:2
#SBATCH --ntasks=2
#SBATCH -o out
#SBATCH -e err
###

#mpirun -n 2 --mca pml ucx -x UCX_TLS=rc,sm,cuda_copy,gdr_copy,cuda_ipc ./main
mpirun -n 2 ./main
